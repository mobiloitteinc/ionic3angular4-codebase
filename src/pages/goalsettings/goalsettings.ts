import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { LocalDbProvider } from '../../providers/local-db/local-db';

@IonicPage()
@Component({
    selector: 'page-goalsettings',
    templateUrl: 'goalsettings.html',
})
export class GoalsettingsPage {
    loading: any;
    userData:any;
    numRegxForDot = (/^(\d+)?([.]?\d{0,1})?$/);
    alertMsg = "";
    bodyFatDiv = false;
    bodyFatErr = false;
    activityLevelDiv = false;
    weightGoalDiv = false;
    paceOfProgressDiv = false;
    workoutRemDiv = false;
    user = {
        bodyfatValue:"",
        actLevel:"Sedentary",
        weightGoalVal:"Lose_fat",
        paceOfProgressVal:"Slowly",
        hrValue:null
    };

    constructor(public navCtrl: NavController, public navParams: NavParams, public nav: Nav, public globalSer: ServicesProvider, public toastCtrl: ToastController, public db: LocalDbProvider, public loader: LoadingController, public alertCtrl: AlertController) {
        this.presentLoading();
        this.db.fetchDataFromUsersTbl(localStorage.getItem("userID")).then(fSucc =>{
            this.loading.dismiss();
            //console.log("fUSucc=> "+JSON.stringify(fSucc));
            this.userData = fSucc;
            this.user.actLevel = this.userData.activityLevel;
            this.user.weightGoalVal = this.userData.weightGoal;
            this.user.paceOfProgressVal = this.userData.paceOfProgress;
        }).catch(fErr =>{
            this.loading.dismiss();
            console.log("fUErr=> "+JSON.stringify(fErr));
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad GoalsettingsPage');
    }

    backBtnFunc(){
        this.navCtrl.pop();
    }

    bodyFatClick(type){
        if(type==1)
            this.bodyFatDiv = true;
        else
            this.bodyFatDiv = false;
    }

    actLevelClickFunc(type){
        if(type==1)
            this.activityLevelDiv = true;
        else
            this.activityLevelDiv = false;
    }

    weightGoalClickFunc(type){
        if(type==1)
            this.weightGoalDiv = true;
        else
            this.weightGoalDiv = false;
    }

    paceProClickFunc(type){
        if(type==1)
            this.paceOfProgressDiv = true;
        else
            this.paceOfProgressDiv = false;
    }

    workOutRemClickFunc(type){
        if(type==1)
            this.workoutRemDiv = true;
        else
            this.workoutRemDiv = false;
    }

    showErr(){
        let toast = this.toastCtrl.create({
            message: this.alertMsg,
            duration: 3000,
            position: 'bottom',
            cssClass: "ff-toast"
        });
        toast.present();
    }

    checkValueFunc(){
        if(this.user.bodyfatValue!=""){
            if(parseFloat(this.user.bodyfatValue)==0){
                this.bodyFatErr = true;
                return;
            }else if(this.user.bodyfatValue=="."){
                this.bodyFatErr = true;
                return;
            }else if(!this.numRegxForDot.test(this.user.bodyfatValue)){
                this.bodyFatErr = true;
                return;
            }else if(parseFloat(this.user.bodyfatValue)>100){
                this.bodyFatErr = true;
                return;
            }else{
                this.bodyFatErr = false;
            }
        }else{
            this.bodyFatErr = false;
        }
    }

    updateFunc(){
        if(this.bodyFatDiv==false && this.activityLevelDiv==false && this.weightGoalDiv==false && this.paceOfProgressDiv==false && this.workoutRemDiv==false){
            console.log("nothing will be updated");
            this.navCtrl.pop();
        }else{
            console.log("else called");
            let clmnArr = [];
            let valueArr = [];
            if(this.bodyFatDiv==true){
                if(this.user.bodyfatValue!=""){
                    if(this.bodyFatErr==true){
                        this.alertMsg = "Enter valid bodyfat percentage.";
                        this.showErr();
                        return;
                    }else{
                        clmnArr.push('isBodyFatPercentageKnown','bodyFatPercentage');
                        valueArr.push('YES',this.user.bodyfatValue);
                    }
                }
            }
            if(this.activityLevelDiv==true){
                clmnArr.push('activityLevel');
                valueArr.push(this.user.actLevel);
            }
            if(this.weightGoalDiv==true){
                clmnArr.push('weightGoal');
                valueArr.push(this.user.weightGoalVal);
            }
            if(this.paceOfProgressDiv==true){
                clmnArr.push('paceOfProgress');
                valueArr.push(this.user.paceOfProgressVal);
            }
            if(this.workoutRemDiv==true){
                if(this.user.hrValue!=null){
                    clmnArr.push('workoutReminder','workoutReminderTime');
                    valueArr.push('YES',this.user.hrValue);
                }
            }
            if(clmnArr.length>0)
                valueArr.push(parseInt(localStorage.getItem("userID")));
            if(clmnArr.length>0){
                this.updateGoalSetting(clmnArr,valueArr);
            }else{
                console.log("nothing will be updated");
                this.navCtrl.pop();
            }
        }
    }

    updateGoalSetting(clmn,val){
        this.presentLoading();
        this.db.updateTbl("Users","userID",clmn,val).then(upSucc =>{
            console.log("upSucc -> "+JSON.stringify(upSucc));
            if(clmn.indexOf('isBodyFatPercentageKnown')!=-1){
                this.insIntoBodyfatTbl(clmn,val);
            }else{
                this.loading.dismiss();
                let upAlert = this.alertCtrl.create({
                    title: 'FLEXIBLE FITNESS',            
                    subTitle: "Your goal setting has been updated successfully.",
                    enableBackdropDismiss: false,
                    buttons: [{
                        text: 'OK',
                        handler: () => {
                            this.navCtrl.pop();
                        }
                    }]        
                });
                upAlert.present();
            }
        }).catch(upErr =>{
            this.loading.dismiss();
            console.log("upErr -> "+JSON.stringify(upErr));
        });
    }

    insIntoBodyfatTbl(cArr, valArr){
        let value = valArr[cArr.indexOf('bodyFatPercentage')];
        console.log("bodyfat value -> "+value);
        let arr = [{
            userID:parseInt(localStorage.getItem("userID")),
            bfpercentage:value,
            bfpercentageCreationTime:new Date().getTime()
        }];
        this.db.insIntoBodyfatListDB(arr).then(insBSucc =>{
            console.log("insBSucc -> "+JSON.stringify(insBSucc));
            this.loading.dismiss();
            let upAlert = this.alertCtrl.create({
                title: 'FLEXIBLE FITNESS',            
                subTitle: "Your goal setting has been updated successfully.",
                enableBackdropDismiss: false,
                buttons: [{
                    text: 'OK',
                    handler: () => {
                        this.navCtrl.pop();
                    }
                }]        
            });
            upAlert.present();
        }).catch(insBErr =>{
            this.loading.dismiss();
            console.log("insBErr -> "+JSON.stringify(insBErr));
        });
    }

    showErrToast(){
        let toast = this.toastCtrl.create({
            message: "Please check you internet connection.",
            duration: 3000,
            position: 'bottom',
            cssClass: "ff-toast"
        });
        toast.present();
    }

    presentLoading(){
        this.loading = this.loader.create({
            spinner: "crescent",
            content: 'Please wait...',
            cssClass: "ff-loader"
        });
        this.loading.present();
    }
}
