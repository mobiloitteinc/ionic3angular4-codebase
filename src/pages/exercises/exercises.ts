import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WorkoutListPage } from '../workout-list/workout-list';
import { AlertController, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-exercises',
    templateUrl: 'exercises.html',
})
export class ExercisesPage {
    actDate:any;
    actName:any;
    actDuration:any;
    actIndex:any;
    numRegx = (/^[0-9]*$/);
    alertMsg = "";

    activityArr = [{
        name:"Activity 1"
    },{
        name:"Activity 2"
    },{
        name:"Activity 3"
    },{
        name:"Activity 4"
    },{
        name:"Activity 5"
    },{
        name:"Activity 6"
    }];

    activityListArr = [
        {
            activity:"Activity 1",
            duration:"60",
            time:"1512449045000"
        },{
            activity:"Activity 2",
            duration:"60",
            time:"1512535445000"
        },{
            activity:"Activity 3",
            duration:"60",
            time:"1512621845000"
        },{
            activity:"Activity 4",
            duration:"60",
            time:"1512708245000"
        },{
            activity:"Activity 5",
            duration:"60",
            time:"1512794645000"
        },{
            activity:"Activity 6",
            duration:"60",
            time:"1512881045000"
        },{
            activity:"Activity 7",
            duration:"60",
            time:"1512967445000"
        }
    ];
    
    constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public toastCtrl: ToastController) {
        if(this.activityListArr.length>0){
            this.actIndex = this.activityListArr.length-1;
            this.actDate = this.activityListArr[this.actIndex].time;
            this.actName = this.activityListArr[this.actIndex].activity;
            this.actDuration = this.activityListArr[this.actIndex].duration;
        }
    }
    ionViewDidLoad() {
       console.log('ionViewDidLoad ExercisesPage');
    }

    backBtnFunc(){
        this.navCtrl.pop();
    }

    goToDetail(){
        let alert = this.alertCtrl.create({
            title: 'How long did you perform this activity?',
            cssClass:"act-dur",
            inputs: [{
                type: 'text',
                name: 'time',
                placeholder: 'Minutes',
                value: ''
            }],
            buttons: [{
                text: 'Cancel',
                role: 'cancel',
                handler: data => {
                  console.log('Cancel clicked');
                }
            },{
                text: 'Submit',
                handler: data => {
                    if(data['time']!=""){
                        if(!this.numRegx.test(data['time'])){
                            this.alertMsg = "Enter valid activity duration.";
                            this.showErr();
                            return false;
                        }else if(parseFloat(data['time'])==0 || parseFloat(data['time'])>60){
                            this.alertMsg = "Enter activity duration from 1 and 60 minutes.";
                            this.showErr();
                            return false;
                        }else
                            this.goodWorkFunc();
                    }else{
                        this.alertMsg = "Enter activity duration.";
                        this.showErr();
                        return false;
                    }
                }
            }]
        });
        alert.present();
    }

    showErr(){
        let toast = this.toastCtrl.create({
            message: this.alertMsg,
            duration: 3000,
            position: 'bottom',
            cssClass: "ff-toast"
        });
        toast.present();
    }
    
    goodWorkFunc(){
        let alert = this.alertCtrl.create({
            title: 'FLEXIBLE FITNESS',            
            subTitle: 'Good work!',
            buttons: [{
                text: 'OK',
                handler: () => {
                }
            }]        
        });
        alert.present();
    }

    actChangeFunc(type){
        if(type==1){
            if(this.actIndex>0){
                this.actIndex--;
                this.actDate = this.activityListArr[this.actIndex].time;
                this.actName = this.activityListArr[this.actIndex].activity;
                this.actDuration = this.activityListArr[this.actIndex].duration;
            }
        }
        if(type==2){
            if(this.actIndex<this.activityListArr.length-1){
                this.actIndex++;
                this.actDate = this.activityListArr[this.actIndex].time;
                this.actName = this.activityListArr[this.actIndex].activity;
                this.actDuration = this.activityListArr[this.actIndex].duration;
            }
        }
    }
}
