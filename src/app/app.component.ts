import { Component, ViewChild } from '@angular/core';
import { Platform, AlertController, Nav, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';
import { Network } from '@ionic-native/network';
import { SQLite } from '@ionic-native/sqlite';
import { Device } from '@ionic-native/device';

import { LoginPage } from '../pages/login/login';
import{ LandingScreenPage } from '../pages/landing-screen/landing-screen';
import { ServicesProvider } from '../providers/services/services';
import { TabsPage } from '../pages/tabs/tabs';
import { LocalDbProvider } from '../providers/local-db/local-db';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    //rootPage: any = LandingScreenPage;
    rootPage: any;
    @ViewChild(Nav) nav: Nav;
    constructor(public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, keyboard: Keyboard, public alertCtrl: AlertController, public network: Network, public globalSer: ServicesProvider, public sqlite: SQLite, public db: LocalDbProvider, public loader: LoadingController, private device: Device) {
        platform.ready().then(() => {
            this.globalSer.deviceUUID = this.device.uuid;
            this.globalSer.devicePlatform = this.device.platform;
            console.log("UUID -> "+this.globalSer.deviceUUID+" platform -> "+this.globalSer.devicePlatform);
            if (platform.is("ios")) {
                statusBar.overlaysWebView(true);
            }
            statusBar.styleDefault();
            splashScreen.hide();
            keyboard.hideKeyboardAccessoryBar(false);
            platform.registerBackButtonAction((e)=>this.handleHWBackBtn(e));
            console.log("network=> "+navigator.onLine);
            globalSer.networkStatus = navigator.onLine;
            this.network.onConnect().subscribe(() => {
                console.log('network connected');
                globalSer.networkStatus = true;
            });
            this.network.onDisconnect().subscribe(() => {
                console.log('network was disconnected');
                globalSer.networkStatus = false;
            });

            let loading = loader.create({
                spinner: "crescent",
                content: 'Please wait...',
                cssClass: "ff-loader"
            });
            loading.present();
            db.createDB().then(dbSucc =>{
                console.log("db create succ -> "+JSON.stringify(dbSucc));
                loading.dismiss();
                if(localStorage.getItem('first_launch')==null){
                    this.nav.setRoot(LandingScreenPage);                
                    localStorage.setItem('first_launch','false');
                }else{
                    console.log("userID-> "+localStorage.getItem('userID'));
                    if(localStorage.getItem('userID')!=null && localStorage.getItem('userID')!="")
                        this.nav.setRoot(TabsPage);
                    else
                        this.nav.setRoot(LoginPage);
                }
            }).catch(dbErr =>{
                loading.dismiss();
                console.log("db create err -> "+JSON.stringify(dbErr));
            });
        });
    }

    handleHWBackBtn(e){
        let alert = this.alertCtrl.create({
            title: 'FLEXIBLE FITNESS',            
            subTitle: 'Would you like to exit from application?',
            buttons: [{
                text: 'OK',
                handler: () => {
                    this.platform.exitApp();
                }
            },{
                text: 'Cancel',
                handler: () => {
                    console.log("cancel");
                }
            }]        
        });
        alert.present();
    }
}

