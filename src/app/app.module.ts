import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { LoginPageModule } from '../pages/login/login.module';
import { SignupPageModule } from '../pages/signup/signup.module';
import { WelcomePageModule } from '../pages/welcome/welcome.module';
import { UnitOfMeasurementPageModule } from '../pages/unit-of-measurement/unit-of-measurement.module';
import { TallMeasurePageModule } from '../pages/tall-measure/tall-measure.module';
import { WeightMeasurePageModule } from '../pages/weight-measure/weight-measure.module';
import { BodyFatPercentagePageModule } from '../pages/body-fat-percentage/body-fat-percentage.module';
import { EnterBodyFatPageModule } from '../pages/enter-body-fat/enter-body-fat.module';

@NgModule({
    declarations: [
        MyApp
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        IonicModule.forRoot(MyApp,{ 
            tabsPlacement:'top', 
            autoFocusAssist: true,
            scrollPadding: true, 
            scrollAssist: true,
            platforms:{
                ios:{
                    statusbarPadding: true
                }
            }
        }),
        LoginPageModule,
        SignupPageModule,
        WelcomePageModule,
        UnitOfMeasurementPageModule,
        TallMeasurePageModule,
        WeightMeasurePageModule,
        BodyFatPercentagePageModule,
        EnterBodyFatPageModule,        
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Keyboard,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        Camera,
        ActionSheet,
    ]
})
export class AppModule { }
