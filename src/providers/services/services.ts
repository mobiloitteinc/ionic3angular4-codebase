import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ServicesProvider {
    baseUrl = "https://www.google.com/";
    header: any = {
        'Content-Type': 'application/json',
        "Authorization": "Basic " + btoa('admin' + ":" + 'admin@123')
    }
    signupUserName:any;
    serData:any;
    networkStatus:any;
    currUser:any = {};
    sqlObj:any;
    userData = {
        gender:"",
        unitOfMeasure:"",
        tallFtValue:"",
        tallInchValue:"",
        tallCMValue:""
    };
    currWorkout = {
        name: ""
    };
    nutritionQueStatus = false;
    deviceUUID = "";
    devicePlatform = "";
    constructor(public http: HttpClient) {
        console.log('Hello ServicesProvider Provider');
    }    

    postMethod(data,apiName){
        return new Promise((resolve, reject) => {
            let url = this.baseUrl + apiName;
            this.http.post(url, data, { headers: this.header }).toPromise()
            .then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })
        });
    }

    getMethod(apiName){
        return new Promise((resolve, reject) => {
            let url = this.baseUrl + apiName;
            this.http.get(url, { headers: this.header }).toPromise()
            .then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })
        });
    }
}
